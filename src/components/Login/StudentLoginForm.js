import React, { Component } from "react";
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';

import CustomPicker from '../Common/CustomPicker';
import AlertModal from '../Common/AlertModal';
import { getData } from '../../common/utils';

class StudentLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: [],
      assessment: [],
      selectedCenterDetails: {},
      batchData: {},
      selectedBatch: [],
      selectedCenter: "",
      selectedAssessment: "",
      username: "",
      password: "",
      error: false,
      validationError: false
    }
  }

  async componentDidMount() {
    try {
      const assessment = await getData('assessmentID');
      const center = await getData('center');
      const batchData = await getData('batchData');

      const selectedAssessment = assessment[0].id;
      const selectedCenter = center[0].id;
      const selectedCenterDetails = center[0];

      this.setState({ center, assessment, selectedCenter, selectedAssessment, batchData, selectedBatch: batchData[0], selectedCenterDetails });
    } catch (e) {
      console.log(e);
    }
  }

  onChangeCenter = (itemValue, itemIndex) => {
    if (itemValue != "0") {
      const { batchData, assessment, center } = this.state;
      this.setState({ selectedCenter: itemValue, selectedBatch: batchData[itemIndex], selectedAssessment: assessment[itemIndex].id, selectedCenterDetails: center[itemIndex] });
    }
  }

  onChangeAssessment = (itemValue, itemIndex) => {
    if (itemValue != "0") {
      const { batchData, center } = this.state;
      this.setState({ selectedAssessment: itemValue, selectedBatch: batchData[itemIndex], selectedCenter: center[itemIndex].id, selectedCenterDetails: center[itemIndex] });
    }
  }

  onChangeUsername = (value) => {
    this.setState({ username : value });
  }

  onChangePassword = (value) => {
    this.setState({ password : value });
  }

  loginStudent = () => {

    const { selectedCenter, selectedAssessment, selectedBatch, username, password, selectedCenterDetails } = this.state;
    
    this.setState({ error: false });
  
    if (!selectedCenter || !selectedAssessment || !username.trim() || !password.trim()) {
      this.setState({ error: true });
      return;
    }
  
    let validationError = true;
    selectedBatch.students.forEach((student) => {
      if (username === student.username && password === student.password) {
        validationError = false;
        const details = {
          traineeName: student.name,
          trainingCenterName: selectedCenterDetails.center_name,
          centerID: this.state.selectedCenter,
          batchID: this.state.selectedBatch.id,
          assessmentID: this.state.selectedAssessment,
          candidateID: student.candidate_id
        };
        this.props.navigation.navigate("TestInfo", { details });
      }
    });
  
    this.setState({ validationError, username: "", password: "" });
  }

  render() {
    return (
      <View>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.fieldContainer}>
            <CustomPicker 
              selectedValue={this.state.selectedCenter}
              options={this.state.center}
              placeholder="Select Center"
              onChange={this.onChangeCenter}
            />
          </View>
          <View style={styles.fieldContainer}>
            <CustomPicker 
              selectedValue={this.state.selectedAssessment}
              options={this.state.assessment}
              placeholder="Select Assessment"
              onChange={this.onChangeAssessment}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Username</Text>
          </View>
          <View style={{ flex: 0.7 }}>
            <TextInput
              placeholder="Enter Here..."
              style={{ height: 50 }}
              onChangeText={this.onChangeUsername}
              value={this.state.username}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Password</Text>
          </View>
          <View style={{ flex: 0.7 }}>
            <TextInput
              placeholder="Enter Here..."
              secureTextEntry={true}
              style={{ height: 50 }}
              onChangeText={this.onChangePassword}
              value={this.state.password}
            />
          </View>
        </View>
        {this.state.error && 
          (
            <View style={styles.errorContainer}>
              <Text style={{ color: 'red' }}>
                Please enter all the fields.
              </Text>
            </View>
          )
        }

        {this.state.validationError && 
          <AlertModal 
            type="One"
            label="Error" 
            message={"Incorrect Username or Password"}
            onPress={() => this.setState({ validationError: false })} 
          />
        }

        <View style={styles.buttonContainer}>
          <Button 
            buttonStyle={styles.button}
            title="Student Login"
            onPress={this.loginStudent}
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.props.changeLoginType('assessor')}  
          >
            <Text style={{ color: '#2975a0', fontWeight: 'bold' }}>
              Assessor Login
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = {
  fieldContainer: {
    borderColor: '#D3D3D3',
    borderWidth : 1,
    elevation: 0.5
  },
  label: {
    flex: 0.3,
    fontSize: 22,
    paddingTop: 14,
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonContainer: {
    marginTop: 20
  },
  errorContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  }
}

export default withNavigation(StudentLoginForm);