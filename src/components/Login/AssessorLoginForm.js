import React, { Component } from "react";
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';

import CustomPicker from '../Common/CustomPicker';
import AlertModal from '../Common/AlertModal';
import { getData } from '../../common/utils';

class AssessorLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: [],
      assessment: [],
      assessorData: [],
      selectedAssessorData: {},
      selectedCenter: "",
      selectedAssessment: "",
      username: "",
      password: "",
      error: false,
      validationError: false,
    }
  }

  componentDidMount() {
    this.load();
    this.props.navigation.addListener('willFocus', this.load);
  }

  load = async () => {
    try {
      const assessment = await getData('assessmentID');
      const center = await getData('center');
      const assessorData = await getData('assessorData');
      const selectedAssessment = assessment[0].id;
      const selectedCenter = center[0].id;

      this.setState({ center, assessment, selectedCenter, selectedAssessment, assessorData, selectedAssessorData: assessorData[0] });
    } catch (e) {
      console.log(e);
    }
  }

  onChangeCenter = (itemValue, itemIndex) => {
    if (itemValue != "0") {
      const { assessorData, assessment } = this.state;
      this.setState({ selectedCenter: itemValue, selectedAssessorData: assessorData[itemIndex], selectedAssessment: assessment[itemIndex].id });
    }
  }

  onChangeAssessment = (itemValue, itemIndex) => {
    if (itemValue != "0") {
      const { assessorData, center } = this.state;
      this.setState({ selectedAssessment: itemValue, selectedAssessorData: assessorData[itemIndex], selectedCenter: center[itemIndex].id });
    }
  }

  onChangeUsername = (value) => {
    this.setState({ username : value });
  }

  onChangePassword = (value) => {
    this.setState({ password : value });
  }

  loginAssessor = () => {
    const { selectedCenter, selectedAssessment, selectedAssessorData, username, password } = this.state;

    this.setState({ error: false });

    if (!selectedCenter || !selectedAssessment || !username.trim() || !password.trim()) {
      this.setState({ error: true });
      return;
    }

    let validationError = true;
    if (username === selectedAssessorData.id && password === selectedAssessorData.password) {
      validationError = false;
      this.props.navigation.navigate("Dashboard", {
        centerID: selectedCenter,
        assessmentID: selectedAssessment
      });
    }

    this.setState({ validationError, username: "", password: "" });
  }

  render() {
    return (
      <View>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.fieldContainer}>
            <CustomPicker 
              selectedValue={this.state.selectedCenter}
              options={this.state.center}
              placeholder="Select Center"
              onChange={this.onChangeCenter}
            />
          </View>
          <View style={styles.fieldContainer}>
            <CustomPicker 
              selectedValue={this.state.selectedAssessment}
              options={this.state.assessment}
              placeholder="Select Assessment"
              onChange={this.onChangeAssessment}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Username</Text>
          </View>
          <View style={{ flex: 0.7 }}>
            <TextInput
              placeholder="Enter Here..."
              style={{ height: 40 }}
              onChangeText={this.onChangeUsername}
              value={this.state.username}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Password</Text>
          </View>
          <View style={{ flex: 0.7 }}>
            <TextInput
              placeholder="Enter Here..."
              secureTextEntry={true}
              style={{ height: 40 }}
              onChangeText={this.onChangePassword}
              value={this.state.password}
            />
          </View>
        </View>
        {this.state.error && 
          (
            <View style={styles.errorContainer}>
              <Text style={{ color: 'red' }}>
                Please enter all the fields.
              </Text>
            </View>
          )
        }
        <View style={styles.buttonContainer}>
          <Button 
            title="Assessor Login"
            buttonStyle={styles.button}
            onPress={this.loginAssessor}  
          />
        </View>
        <View style={styles.linkContainer}>
          <TouchableOpacity
            onPress={() => this.props.changeLoginType('student')}  
          >
            <Text style={{ color: '#2975a0', fontWeight: 'bold' }}>
              Student Login
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.props.goToVerify}  
          >
            <Text style={{ color: '#2975a0', fontWeight: 'bold' }}>
              Download Assessment
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.validationError && 
          <AlertModal 
            type="One"
            label="Error" 
            message={"Incorrect Username or Password"}
            onPress={() => this.setState({ validationError: false })} 
          />
        }
      </View>
    )
  }
}

export default withNavigation(AssessorLoginForm);

const styles = {
  fieldContainer: {
    borderColor: '#D3D3D3',
    borderWidth : 1,
    elevation: 0.5
  },
  label: {
    flex: 0.3,
    paddingTop: 8,
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonContainer: {
    marginTop: 20
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  },
  linkContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  errorContainer: {
    alignItems: 'center',
    marginTop: 20,
  }
}