import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
  return (
    <View style={styles.header}>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.title}>{props.headerTitle}</Text>
      </View>
      {props.children}
    </View>
  )
}

const styles = {
  header: {
    backgroundColor: '#2975a0',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  title: {
    color: '#fff',
    fontSize: 18
  }
}

export default Header;