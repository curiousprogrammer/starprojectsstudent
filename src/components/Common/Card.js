import React from 'react';
import { View } from 'react-native';

const Card = (props) => {
  return (
    <View style={[styles.cardStyles, { ...props.style }]}>
      {props.children}
    </View>
  )
}

const styles = {
  cardStyles: {
    borderColor: '#D3D3D3',
    elevation: 0.8,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30
  }
}

export default Card;