import React, { Component } from "react";
import { View, Text, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import AlertModal from '../components/Common/AlertModal';
import { downloadAssessment } from '../actions/downloadActions';

class DownloadAssessmentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assessmentID: "",
      error: false,
      validationError: false,
      errorMsg: "",
      downloaded: false
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const { error, errorMsg, downloaded } = this.props;
    if (error && error !== prevProps.error) {
      this.setState({ validationError: true, errorMsg });
    }

    if (downloaded && downloaded !== prevProps.downloaded) {
      this.setState({ downloaded: true });
    }
  }

  onChangeAssessmentID = (assessmentID) => {
    this.setState({ assessmentID });
  }

  downloadAssessment = () => {
    const { assessmentID } = this.state;

    if (!assessmentID.trim()) {
      this.setState({ error: true });
      return;
    }

    this.props.downloadAssessment(assessmentID, this.props.centerObj);
  }

  renderCenterDetails() {
    const { centerDetails } = this.props;
    return (
      <View>
        <View style={styles.centerDetails}>
          <Text style={styles.strong}>Center Name: </Text>
          <Text style={styles.text}>{centerDetails.center_name}</Text>
        </View>
        <View style={styles.centerDetails}>
          <Text style={styles.strong}>Contact Person: </Text>
          <Text style={styles.text}>{centerDetails.center_owner_name}</Text>
        </View>
        <View style={styles.centerDetails}>
          <Text style={styles.strong}>Email: </Text>
          <Text style={styles.text}>{centerDetails.center_email}</Text>
        </View>
        <View style={styles.centerDetails}>
          <Text style={styles.strong}>Address: </Text>
          <Text style={styles.text}>{centerDetails.center_address}, {centerDetails.center_city}</Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ marginBottom: 40 }}>
          <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
            <View style={styles.label}>
              <Text>Assessment ID</Text>
            </View>
            <View style={{ flex: 0.5 }}>
              <TextInput
                placeholder="Enter Here..."
                style={{ height: 40 }}
                onChangeText={this.onChangeAssessmentID}
                value={this.state.assessmentID}
              />
            </View>
          </View>
        </View>
        {this.renderCenterDetails()}
        {this.state.error && 
          (
            <View style={styles.errorContainer}>
              <Text style={{ color: 'red' }}>
                Please enter Assessment ID.
              </Text>
            </View>
          )
        }
        <View style={styles.buttonContainer}>
          <Button 
            buttonStyle={styles.button}
            title="Change Center"
            onPress={this.props.setVerified}
          />
          <Button
            buttonStyle={styles.button}
            title="Download Assessment"
            onPress={this.downloadAssessment}
          />
        </View>

        {this.state.validationError && 
          <AlertModal 
            type="One"
            label="Error" 
            message={this.state.errorMsg}
            onPress={() => this.setState({ validationError: false })} 
          />
        }

        {this.state.downloaded && 
          <AlertModal 
            type="One"
            label="Success" 
            message="Assessment and Batch downloaded Successfully"
            onPress={() => this.props.navigate("Login")} 
          />
        }
      </View>
    )
  }
}

const mapStateToProps = ({ verification, download }) => {
  return {
    centerObj: verification.centerObj,
    downloaded: download.downloaded,
    error: download.error,
    errorMsg: download.errorMsg
  }
}

export default connect(mapStateToProps, { downloadAssessment })(DownloadAssessmentForm);

const styles = {
  container: {
    width: '50%',
    marginTop: 10,
    marginBottom: 10
  },
  fieldContainer: {
    borderColor: '#D3D3D3',
    borderWidth : 1,
    elevation: 0.5
  },
  strong: {
    fontWeight: "bold"
  },
  label: {
    flex: 0.5,
    paddingTop: 8,
    paddingLeft: 10,
    paddingRight: 10
  },
  text: {
    flex: 1, 
    flexWrap: 'wrap'
  },
  centerDetails: {
    borderColor: '#D3D3D3',
    borderWidth : 1,
    elevation: 0.5,
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonContainer: {
    marginTop: 20
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 10
  },
  errorContainer: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20
  }
}