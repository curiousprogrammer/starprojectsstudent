import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import { Card, Button, CheckBox, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';

import globalStyles from '../common/globalStyles';
import Header from '../components/Common/Header';
import FullScreen from '../nativeModules/FullScreen';
import { saveTraineeFeedback } from '../actions/saveActions';
import { getDate } from '../common/utils';

class TraineeFeedback extends Component {
  constructor(props) {
    super(props);
    const { details } = this.props.navigation.state.params;

    const today = new Date();
    const stringDate = getDate(today);

    this.state = {
      showCalendar: false,
      details,
      sectionA: [
        { "label": "The Trainer is Knowledgeable", "key": "about_trainer_training_1" },
        { "label": "Adequate time is provided for questions & discussion & clearing doubts", "key": "about_trainer_training_2" },
        { "label": "The topics mentioned in the course outline are tought in the class", "key": "about_trainer_training_3" },
        { "label": "The trainer demonstrates the use of Tools & Equipment to conduct the training (if applicable)", "key": "about_trainer_training_4" },
        { "label": "Classes are conducted regularly and on time", "key": "about_trainer_training_5" },
      ],
      sectionB: [
        { "label": "The Center and Labs/Workshop is hygienic and safe", "key": "training_vanue_1" },
        { "label": "The Workshop/Lab is good in terms of space, lighting and seating arrangement", "key": "training_vanue_2" }
      ],
      sectionC: [
        { "label": "Were you briefed about PMKVY in the beginning of the course", "key": "pmkvc_1" },
        { "label": "Have you watched the PMKVY Animation video", "key": "pmkvc_2" },
        { "label": "Have you been provided the PMKVY booklet/Pamphlet at the beginning of the course", "key": "pmkvc_3" },
        { "label": "Are you aware of the training course fee being charged", "key": "pmkvc_4" },
        { "label": "Are you aware of the assessment fee that is being charged", "key": "pmkvc_5" },
        { "label": "Are you of the reward money you will receive after successful completion of course", "key": "pmkvc_6" },
        { "label": "Are you aware of the Auto Debit mandate", "key": "pmkvc_7" },
        { "label": "Are you aware of the PMKVY Grievance Portal", "key": "pmkvc_8" },
        { "label": "How did you first come to know about PMKVY?", "key": "pmkvc_9" },
      ],
      sectionD: { "label": "How would you rate the training overall", "key": "training_rate" },
      job_roll_end: "",
      training_start_date: stringDate,
      trainingStartDateCalendar: today,
      trainer_name: "",
      about_trainer_training_1: { 
        stronglyAgree: false,
        agree: false,
        disagree: false,
        stronglyDisagree: false,
        value: ""
      },
      about_trainer_training_2: { 
        stronglyAgree: false,
        agree: false,
        disagree: false,
        stronglyDisagree: false,
        value: ""
      },
      about_trainer_training_3: { 
        stronglyAgree: false,
        agree: false,
        disagree: false,
        stronglyDisagree: false,
        value: ""
      },
      about_trainer_training_4: { 
        stronglyAgree: false,
        agree: false,
        disagree: false,
        stronglyDisagree: false,
        value: ""
      },
      about_trainer_training_5: { 
        stronglyAgree: false,
        agree: false,
        disagree: false,
        stronglyDisagree: false,
        value: ""
      },
      training_vanue_1: {
        excellent: false,
        good: false,
        average: false,
        poor: false,
        value: ""
      },
      training_vanue_2: {
        excellent: false,
        good: false,
        average: false,
        poor: false,
        value: ""
      },
      pmkvc_1: { yes: false, no: false, value: "" },
      pmkvc_2: { yes: false, no: false, value: "" },
      pmkvc_3: { yes: false, no: false, value: "" },
      pmkvc_4: { yes: false, no: false, value: "" },
      pmkvc_5: { yes: false, no: false, value: "" },
      pmkvc_6: { yes: false, no: false, value: "" },
      pmkvc_7: { yes: false, no: false, value: "" },
      pmkvc_8: { yes: false, no: false, value: "" },
      pmkvc_9: "",
      training_rate: {
        excellent: false,
        good: false,
        average: false,
        poor: false,
        value: ""
      }
    }
    FullScreen.enable();
  }

  onDateSelected = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.date;
    const stringDate = getDate(currentDate);
    this.setState({ trainingStartDateCalendar: currentDate, training_start_date: stringDate, showCalendar: false });
    FullScreen.enable();
  };

  onSelectOption = (key, checked, value) => {
    const toSetObj = this.state[key];

    for (const property in toSetObj) {
      toSetObj[property] = false;
    }

    toSetObj[checked] = true;
    toSetObj.value = value;

    this.setState({ [key]: toSetObj });
  }

  onSubmit = () => {
    const { 
      details,
      job_roll_end,
      training_start_date,
      trainer_name,
      about_trainer_training_1, 
      about_trainer_training_2,
      about_trainer_training_3, 
      about_trainer_training_4, 
      about_trainer_training_5,
      training_vanue_1,
      training_vanue_2,
      pmkvc_1,
      pmkvc_2,
      pmkvc_3,
      pmkvc_4,
      pmkvc_5,
      pmkvc_6,
      pmkvc_7,
      pmkvc_8,
      pmkvc_9,
      training_rate
    } = this.state;

    const feedbackObj = {
      candidateID: details.candidateID,
      trainee_name: details.traineeName,
      trainig_center_name: details.trainingCenterName,
      job_roll_end,
      center_id: details.centerID,
      training_start_date,
      batch_id: details.batchID,
      student_enrolment_no: details.candidateID,
      trainer_name,
      about_trainer_training_1: about_trainer_training_1.value,
      about_trainer_training_2: about_trainer_training_2.value,
      about_trainer_training_3: about_trainer_training_3.value,
      about_trainer_training_4: about_trainer_training_4.value,
      about_trainer_training_5: about_trainer_training_5.value,
      training_vanue_1: training_vanue_1.value,
      training_vanue_2: training_vanue_2.value,
      pmkvc_1: pmkvc_1.value,
      pmkvc_2: pmkvc_2.value,
      pmkvc_3: pmkvc_3.value,
      pmkvc_4: pmkvc_4.value,
      pmkvc_5: pmkvc_5.value,
      pmkvc_6: pmkvc_6.value,
      pmkvc_7: pmkvc_7.value,
      pmkvc_8: pmkvc_8.value,
      pmkvc_9,
      training_rate: training_rate.value
    }

    this.props.saveTraineeFeedback(feedbackObj);
    this.props.navigation.navigate("Login");
  }

  renderSectionA = () => {
    return (
      <View>
        <Text style={styles.headerLabel}>About Trainer and Training (Kindly tick one option)</Text>
        <View>
        {this.state.sectionA.map((item, index) => {
          return (
            <View key={index}>
              <View>
                <Text style={styles.labelText}>{index + 1}. {item.label}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].stronglyAgree}
                    onPress={() => this.onSelectOption(item.key, "stronglyAgree", 1)}
                  />
                  <Text style={styles.checkboxText}>Strongly {"\n"} Agree</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].agree}
                    onPress={() => this.onSelectOption(item.key, "agree", 2)}
                  />
                  <Text style={styles.checkboxText}>Agree</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].disagree}
                    onPress={() => this.onSelectOption(item.key, "disagree", 3)}
                  />
                  <Text style={styles.checkboxText}>Disagree</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].stronglyDisagree}
                    onPress={() => this.onSelectOption(item.key, "stronglyDisagree", 4)}
                  />
                  <Text style={styles.checkboxText}>Strongly {"\n"} Disagree</Text>
                </View>
              </View>
            </View>
          )
        })}
        </View>
      </View>
    )
  }

  renderSectionB = () => {
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.headerLabel}>About the training venue (Kindly tick one option)</Text>
        <View>
          {this.state.sectionB.map((item, index) => {
          return (
            <View key={index}>
              <View>
                <Text style={styles.labelText}>{index + 1}. {item.label}</Text>
              </View>
              <View style={{  flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].excellent}
                    onPress={() => this.onSelectOption(item.key, "excellent", 1)}
                  />
                  <Text style={styles.checkboxText}>Excellent</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].good}
                    onPress={() => this.onSelectOption(item.key, "good", 2)}
                  />
                  <Text style={styles.checkboxText}>Good</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].average}
                    onPress={() => this.onSelectOption(item.key, "average", 3)}
                  />
                  <Text style={styles.checkboxText}>Average</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <CheckBox
                    containerStyle={styles.checkboxContainer}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state[item.key].poor}
                    onPress={() => this.onSelectOption(item.key, "poor", 4)}
                  />
                  <Text style={styles.checkboxText}>Poor</Text>
                </View>
              </View>
            </View>
          )
        })}
        </View>
      </View>
    )
  }

  renderSectionC = () => {
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.headerLabel}>Awareness on PMKVY (Kindly tick one option)</Text>
        <View>
          {this.state.sectionC.map((item, index) => {
          return (
            <View key={index}>
              <View>
                <Text style={styles.labelText}>{index + 1}. {item.label}</Text>
              </View>
              {item.key === 'pmkvc_9' ? (
                <View>
                  <Input
                    placeholder="Enter here..."
                    value={this.state.pmkvc_9}
                    onChangeText={value => this.setState({ pmkvc_9: value })}
                  />
                </View>
              ) : (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <CheckBox
                      containerStyle={styles.checkboxContainer}
                      checkedIcon='dot-circle-o'
                      uncheckedIcon='circle-o'
                      checked={this.state[item.key].yes}
                      onPress={() => this.onSelectOption(item.key, "yes", 1)}
                    />
                    <Text style={[styles.checkboxText]}>Yes</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <CheckBox
                      containerStyle={styles.checkboxContainer}
                      checkedIcon='dot-circle-o'
                      uncheckedIcon='circle-o'
                      checked={this.state[item.key].no}
                      onPress={() => this.onSelectOption(item.key, "no", 0)}
                    />
                    <Text style={[styles.checkboxText, { paddingRight: 20 }]}>No</Text>
                  </View>
                </View>
              )
              }
            </View>
          )
        })}
        </View>
      </View>
    )
  }

  renderSectionD = () => {
    const { sectionD } = this.state;
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.headerLabel}>(Kindly tick one option)</Text>
        <View>
          <View>
            <View>
              <Text style={styles.labelText}>1. {sectionD.label}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <CheckBox
                  containerStyle={styles.checkboxContainer}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                  checked={this.state[sectionD.key].excellent}
                  onPress={() => this.onSelectOption(sectionD.key, "excellent", 1)}
                />
                <Text style={styles.checkboxText}>Excellent</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <CheckBox
                  containerStyle={styles.checkboxContainer}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                  checked={this.state[sectionD.key].good}
                  onPress={() => this.onSelectOption(sectionD.key, "good", 2)}
                />
                <Text style={styles.checkboxText}>Good</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <CheckBox
                  containerStyle={styles.checkboxContainer}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                  checked={this.state[sectionD.key].average}
                  onPress={() => this.onSelectOption(sectionD.key, "average", 3)}
                />
                <Text style={styles.checkboxText}>Average</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <CheckBox
                  containerStyle={styles.checkboxContainer}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                  checked={this.state[sectionD.key].poor}
                  onPress={() => this.onSelectOption(sectionD.key, "poor", 4)}
                />
                <Text style={styles.checkboxText}>Poor</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={globalStyles.container}>
        <Header headerTitle="Training Feedback Form - PMKVY"></Header>
        <ScrollView>
          <Card>
          <View>
            <View>
              <Text>Job Role Enrolled</Text>
              <Input
                placeholder="Enter here..."
                value={this.state.job_roll_end}
                onChangeText={value => this.setState({ job_roll_end: value })}
              />
            </View>
            <View>
              <Text>Training Start Date</Text>
              <Input
                placeholder="Enter here..."
                disabled={true}
                rightIcon={
                  <Icon
                    name='calendar'
                    reverse
                    reverseColor="#6584b8"
                    size={24}
                    onPress={() => this.setState({ showCalendar: true })}
                  />
                }
                value={this.state.training_start_date}
                // onChange={(value) => this.setState({ training_start_date: value })}
              />
              {this.state.showCalendar && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={this.state.trainingStartDateCalendar}
                  mode={"date"}
                  display="default"
                  onChange={this.onDateSelected}
                />
              )}
            </View>
            <View>
              <Text>Trainer's name</Text>
              <Input
                placeholder="Enter here..."
                value={this.state.trainer_name}
                onChangeText={value => this.setState({ trainer_name: value })}
              />
            </View>
          </View>
            {this.renderSectionA()}
            {this.renderSectionB()}
            {this.renderSectionC()}
            {this.renderSectionD()}
            <Button
              title="SUBMIT"
              buttonStyle={styles.button}
              containerStyle={{ marginTop: 50 }}
              onPress={this.onSubmit}
            />
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default connect(null, { saveTraineeFeedback })(TraineeFeedback);

const styles = {
  sectionContainer: {
    marginTop: 50
  },
  headerLabel: {
    backgroundColor: '#6584b8',
    color: '#fff',
    fontSize: 20,
    marginBottom: 10,
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  checkboxText: {
    fontSize: 18,
    paddingTop: 13
  },
  labelText: {
    fontSize: 16,
    marginTop: 15
  },
  checkboxContainer: {
    backgroundColor: "transparent", 
    borderWidth: 0
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15
  }
}