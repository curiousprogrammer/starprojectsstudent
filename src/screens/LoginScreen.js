import React, { Component } from 'react';
import { View, Image } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import Card from '../components/Common/Card';
import StudentLoginForm from '../components/Login/StudentLoginForm';
import AssessorLoginForm from '../components/Login/AssessorLoginForm';
import globalStyles from '../common/globalStyles';
import FullScreen from '../nativeModules/FullScreen';

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loginType: 'student'
    }
  }

  componentDidMount() {
    SplashScreen.hide();
    this.props.navigation.addListener('willFocus', () => {
      FullScreen.disable();
    });
  }

  changeLoginType = (loginType) => {
    this.setState({ loginType });
  }

  goToVerify = () => {
    this.props.navigation.navigate("Verification");
  }

  render() {
    return (
      <View style={globalStyles.container}>
        <Card style={{ marginTop: 50 }}>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              source={require('../../assets/img/logo.png')}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            {this.state.loginType === 'student' ? 
            (
              <StudentLoginForm 
                changeLoginType={this.changeLoginType}
              />
            ) : (
              <AssessorLoginForm 
                goToVerify={this.goToVerify}
                changeLoginType={this.changeLoginType}
              />
            )}
          </View>
        </Card>
      </View>

    )
  }
}

const styles = {
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 400,
    height: 250,
    resizeMode: 'contain'
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  }
}

export default LoginScreen;