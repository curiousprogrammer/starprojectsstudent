import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import gs from '../common/globalStyles';
import FullScreen from '../nativeModules/FullScreen';
import { getAssessmentData } from '../common/utils';
import { saveAssessmentDetails } from '../actions/testActions';
import { resetSavedImage } from '../actions/saveActions';

class TestInfoScreen extends Component {
  constructor(props) {
    super(props);
    
    const { details } = this.props.navigation.state.params;

    this.state = {
      centerID: details.centerID,
      candidateID: details.candidateID,
      assessmentID: details.assessmentID,
      details,
      testData: null,
      savedImage: false
    }
  }
  
  async componentDidMount() {
    FullScreen.enable();
    
    const { centerID, assessmentID } = this.state;

    try {
      const testData = await getAssessmentData('assessmentData', centerID, assessmentID);
      this.setState({ testData: testData });

      const assessmentDetails = {
        centerID,
        assessmentID,
        assessmentName: testData.assessment.assessment_name,
        assessmentDuration: testData.assessment.assessment_duration
      }

      this.props.saveAssessmentDetails(assessmentDetails);

    } catch (e) {
      console.log(e);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { savedImage } = this.props;

    if (savedImage && savedImage !== prevProps.savedImage) {
      this.setState({ savedImage: true });
    }
  }

  componentWillUnmount() {
    this.props.resetSavedImage();
  }

  startAssessment = () => {
    this.props.navigation.navigate("Test", { testData: this.state.testData, details: this.state.details });
    this.props.resetSavedImage();
  }

  render() {
    return (
      <View style={gs.container}>
        <View style={{ marginTop: 50 }}>
          <Text style={styles.header}>
            About Submission:
          </Text>
        </View>
        <View style={{ marginTop: 50 }}>
          <Text style={styles.instructions}>
            1. Your answers are saved whenever you navigate between questions.
          </Text>
          <Text style={styles.instructions}>
            2. After all questions are attempted to your satisfaction you have to click on the 'SUBMIT' button. You should be careful and ensure you are ready to submit the paper before the required test time. Once the exam duration expires you will not be able to attempt any questions or check the answers and the test will be auto-submitted basis the options you had selected upto the time expiry.
          </Text>
          <Text style={styles.instructions}>
            3. You can make changes in your choice of answer only before clicking the 'SUBMIT' button.
          </Text>
          <Text style={styles.instructions}>
            4. Your answers would be saved automatically by the computer system even if you have not clicked the 'SUBMIT' button.
          </Text>
          <Text style={styles.instructions}>
            5. You will see 3 warnings before the final submission. Before the 3rd warning you may go back to the test in case you want to re-check your answer.
          </Text>
        </View>
        <View style={{ marginTop: 40 }}>
          <Text style={styles.header}>ALL THE BEST!</Text>
        </View>
        <View style={{ marginTop: 40 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Button 
              title="Capture Photo"
              containerStyle={{ marginRight: 100 }}
              buttonStyle={styles.button}
              onPress={() => this.props.navigation.navigate("Camera", 
              { 
                type: 'Student', 
                candidateID: this.state.candidateID,
                assessmentID: this.state.assessmentID
              }
              )}
            />
            <Button
              title="Start Assessment"
              buttonStyle={styles.button}
              disabled={!this.state.savedImage}
              onPress={this.startAssessment}
            />
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = ({ save }) => {
  return {
    savedImage: save.savedImage
  }
}

export default connect(mapStateToProps, { saveAssessmentDetails, resetSavedImage })(TestInfoScreen);

const styles = {
  button: {
    height: 50,
    width: 300
  },
  header: {
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 26
  },
  instructions: {
    fontSize: 18,
    marginBottom: 20
  }
}