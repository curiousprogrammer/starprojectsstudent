import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity, SafeAreaView, Dimensions, BackHandler } from 'react-native';
import { CheckBox, Button } from 'react-native-elements';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import BackgroundTimer from 'react-native-background-timer';

import { saveStudentImage, saveTheoryAnswers, resetTestSaved, saveCompletedStudent } from '../actions/saveActions';
import globalStyles from '../common/globalStyles';
import Header from '../components/Common/Header';
import CustomPicker from '../components/Common/CustomPicker';
import Timer from '../components/Common/Timer';
import Card from '../components/Common/Card';
import Swipe from '../components/Common/Swipe';
import FullScreen from '../nativeModules/FullScreen';
import AlertModal from '../components/Common/AlertModal';
class TestScreen extends Component {

  constructor(props) {
    super(props);

    const { testData: { assessment }, details } = this.props.navigation.state.params;
    const assessmentDuration = assessment.assessment_duration;
    const languages = assessment.language;
    const theory_question = assessment.theory_question;
    const numberOfQuestions = theory_question.length;

    this.state = {
      details,
      candidateID: details.candidateID,
      assessmentID: details.assessmentID,
      centerID: details.centerID,
      assessmentDuration,
      assessmentData: theory_question,
      selectedLanguage: languages[0].id,
      selectedLanguageName: languages[0].language_name,
      languages,
      activeQuestion: 0,
      numberOfQuestions,
      imageCapture: false,
      startQuestionTimer: false,
      sec: 0,
      testSaved: false,
      endConfirm: false
    }

    this.cameraInterval = null;
    this.questionInterval = null;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.setState({ imageCapture: true, startQuestionTimer: true }, () => {
      this.handleStart();
    });
    this.props.resetTestSaved();
  }

  componentDidUpdate(prevProps, prevState) {
    const { testSaved } = this.props;

    if (testSaved && testSaved != prevProps.testSaved) {
      this.setState({ testSaved });
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
    clearInterval(this.cameraInterval);
    clearInterval(this.questionInterval);
  }

  handleBackButton() {
    return true;
  }

  handleStart = () => {
    if (this.state.imageCapture) {
      this.cameraInterval = setInterval(() => {
        this.takePicture();
      }, 60000);
    } else {
      clearInterval(this.cameraInterval);
    }

    if(this.state.startQuestionTimer) {
      this.questionInterval = BackgroundTimer.setInterval(() => {
        this.state.sec = ++this.state.sec
      }, 1000);
    } else {
      clearInterval(this.questionInterval);
    }
  };

  handleReset = () => {
    this.setState({
      sec: 0,
      start: false
    });
  };

  takePicture = async () => {
    const { candidateID, assessmentID } = this.state;
    if (this.camera) {
      const options = { quality: 0.5, base64: true, doNotSave: true };
      const data = await this.camera.takePictureAsync(options);

      this.props.saveStudentImage(data.base64, candidateID, assessmentID);
    }
  };

  onChangeLanguage = (itemValue, itemIndex) => {
    this.setState({ selectedLanguage: itemValue });

    const { languages } = this.state;

    let changedLanguage;
    languages.forEach((language) => {
      if (language.id === itemValue){
        changedLanguage = language.language_name;
      } 
    });

    this.setState({ 
      selectedLanguage: itemValue,
      selectedLanguageName: changedLanguage
    });
  }

  onSelectOption = (questionIndex, optionIndex) => {
    const assessmentDataCopy = this.state.assessmentData;
    if (assessmentDataCopy[questionIndex].saved) {
      alert('You have already saved this answer!');
      return;
    } else {
      assessmentDataCopy[questionIndex].options.forEach(option => {
        option.checked = false;
      });
      assessmentDataCopy[questionIndex].options[optionIndex].checked = true;
      assessmentDataCopy[questionIndex].selectedOption = optionIndex + 1;

      this.setState({ assessmentData: assessmentDataCopy });
    }
  }

  onSaveQuestion = () => {
    const { activeQuestion, assessmentData } = this.state;
    const assessmentDataCopy = assessmentData;

    const optionLength = assessmentDataCopy[activeQuestion].options.length;
    let uncheckedOptions = 0;
    assessmentDataCopy[activeQuestion].options.forEach(option => {
      if (!option.checked) {
        uncheckedOptions++; 
        }
    });

    const previousTimeTaken = assessmentDataCopy[activeQuestion].timeTaken;

    if (uncheckedOptions == optionLength) {
      alert("Please select an option first!");
      return;
    } else {
      assessmentDataCopy[activeQuestion].saved = true;
      assessmentDataCopy[activeQuestion].timeTaken = previousTimeTaken + this.state.sec;
    }
    
    this.handleReset();
    this.setState({ assessmentData: assessmentDataCopy }, () => {
      this.onSkipQuestion();
    });
  }

  onSkipQuestion = () => {
    const { activeQuestion, numberOfQuestions, assessmentData } = this.state;
    const assessmentDataCopy = assessmentData;
    let newActiveQuestion = activeQuestion;

    if (activeQuestion < numberOfQuestions - 1) {
      newActiveQuestion++;
    } else {
      newActiveQuestion = 0;
    }

    const previousTimeTaken = assessmentDataCopy[activeQuestion].timeTaken;

    assessmentDataCopy[activeQuestion].timeTaken = previousTimeTaken + this.state.sec;
    this.handleReset();
    this.setState({ activeQuestion: newActiveQuestion, assessmentData: assessmentDataCopy });
  }

  endTest = () => {
    this.onSubmit();
  }

  onSubmit = () => {
    this.setState({ endConfirm: false });
    const { assessmentData, candidateID, assessmentID, centerID } = this.state;

    const answersObj = assessmentData.map((question) => {
      return {
        question_id: question.question_id,
        answer_option: question.selectedOption,
        time: question.timeTaken
      }
    });

    this.props.saveTheoryAnswers(answersObj, candidateID, assessmentID);
    this.props.saveCompletedStudent(candidateID, centerID);

    clearInterval(this.cameraInterval);
    clearInterval(this.questionInterval);
  }

  renderCard = (item, index) => {
    return (
      <Card style={styles.testCard}>
        <View key={item.u_question_id}>
          <Text style={styles.question}>{index + 1}. {item.question[this.state.selectedLanguageName]}</Text>
          {item.question_image !== "" && (
            <Image style={styles.questionImage} source={{ uri: item.question_image }} resizeMode="contain" />
          )}
          {item.options.map(({ option, checked }, i) => {
            if (option[this.state.selectedLanguageName]) {
              return (
                <View key={i}>
                  <CheckBox
                    containerStyle={{ backgroundColor: "transparent", borderWidth: 0 }}
                    title={option[this.state.selectedLanguageName]}
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={checked}
                    onPress={() => this.onSelectOption(index, i)}
                  />
                </View>
              )
            }
          })}
        </View>
        {!item.saved ?
          (
            <View style={styles.buttonContainer}>
              <Button
                title="Save"
                buttonStyle={styles.button}
                containerStyle={{ marginRight: 20 }}
                onPress={this.onSaveQuestion}
              />
              <Button
                title="Skip"
                buttonStyle={[styles.button, { backgroundColor: 'black' }]}
                onPress={this.onSkipQuestion}
              />
            </View>
          ) : (
            <View>
              <Button
                title="Next"
                buttonStyle={[styles.button, { backgroundColor: 'black' }]}
                onPress={this.onSkipQuestion}
              />
            </View>
          )
        }
      </Card>
    )
  }

  render() {
    FullScreen.enable();
    const { activeQuestion, assessmentData } = this.state;
    return (
        <View style={styles.container}>
          <RNCamera
            ref={(ref) => {
              this.camera = ref;
            }}
            style={styles.preview}
            type={RNCamera.Constants.Type.front}
            flashMode={RNCamera.Constants.FlashMode.on}
            captureAudio={false}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          />
          <View style={{ flex: 0.999 }}>
            <Header headerTitle="All questions are mandatory">
              <View style={globalStyles.spaceBetween}>
                <View style={styles.pickerContainer}>
                  <CustomPicker 
                    selectedValue={this.state.selectedLanguage}
                    options={this.state.languages}
                    placeholder="Select Language"
                    onChange={this.onChangeLanguage}
                    style={styles.pickerStyle}
                  />
                </View>
                <View style={{ top: 35, flexDirection: 'row' }}>
                  <Timer />
                  <Timer duration={this.state.assessmentDuration} endTest={this.endTest} />
                </View>
              </View>
              </Header>

              {assessmentData && assessmentData.length > 0 ?
                (
                  <View style={{ flexDirection: 'row', marginTop: 20, paddingHorizontal: 20 }}>
                    <View style={{ flex: 0.7  }}>
                      <Swipe 
                        data={assessmentData}
                        activeQuestion={activeQuestion}
                        renderCard={(item, i) => this.renderCard(item, i)}
                        onSwipeLeft={(ques, i) => this.setState({ activeQuestion: i })}
                        onSwipeRight={(ques, i) => this.setState({ activeQuestion: i })}
                      />
                      <Button 
                        title="SUBMIT"
                        buttonStyle={styles.submitButton}
                        onPress={() => this.setState({ endConfirm: true })}
                      />
                    </View>
                    <View style={{ flex: 0.3, height: Dimensions.get('window').height }}>
                      <SafeAreaView>
                        <FlatList
                          contentContainerStyle={{ paddingBottom: 150 }}
                          data={assessmentData}
                          renderItem={({ item, index }) => 
                            (
                              <TouchableOpacity 
                                style={[
                                  styles.navigation, 
                                  index === activeQuestion && { backgroundColor: '#1D1D1D' },
                                  item.saved && { backgroundColor: '#32cd32' }
                                ]} 
                                onPress={() => this.setState({ activeQuestion: index })}
                              >
                                <Text style={styles.navigationText}>Question {index+1}</Text>
                              </TouchableOpacity>
                            )
                          }
                          keyExtractor={item => item.u_question_id}
                        />
                      </SafeAreaView>
                    </View>
                  </View>
                ) : (
                  <View>
                    <Text style={styles.noQuestions}>No questions available</Text>
                  </View>
                )
              }

              {this.state.testSaved && 
                <AlertModal 
                  type="One"
                  label="Success" 
                  message="You have completed the test!"
                  onPress={() => {
                    this.props.navigation.navigate("TraineeFeedback", { details: this.state.details });
                  }} 
                />
              }

              {this.state.endConfirm && 
                <AlertModal 
                  type="Two"
                  label="Confirmation" 
                  message="Are you sure to submit the assessment?"
                  onPress={this.onSubmit} 
                  onCancelPress={() => this.setState({ endConfirm: false })}
                />
              }
          </View>
          
        </View>
    )
  }
}

const mapStateToProps = ({ save }) => {
  return {
    testSaved: save.testSaved
  }
}

export default connect(mapStateToProps, { saveStudentImage, saveTheoryAnswers, resetTestSaved, saveCompletedStudent })(TestScreen);

const styles = {
  preview: {
    flex: 0.001,
    justifyContent: 'flex-start',
    opacity: 0,
  },
  container: {
    flex: 1
  },
  pickerContainer: {
    backgroundColor: '#c7c7c7',
    marginTop: 30,
    width: 115
  },
  pickerStyle: {
    height: 25,
    width: 125,
  },
  timer: {
    color: '#fff'
  },
  testCard: {
    backgroundColor: '#fff',
  },
  question: {
    fontSize: 20
  },
  questionImage: {
    height: 200,
    width: 300,
    alignSelf: 'center'
  },
  button: {
    width: 100
  },
  submitButton: {
    backgroundColor: '#32cd32',
    borderRadius: 10,
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: Dimensions.get('window').width / 1.5
  },
  buttonContainer: { 
    flexDirection: 'row', 
    marginTop: 20
  },
  navigation: {
    backgroundColor: '#2975a0',
    borderRadius: 10,
    height: 35,
    marginBottom: 15,
    marginLeft: 20,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 10
  },
  navigationText: {
    color: 'white',
    fontSize: 16
  },
  noQuestions: {
    fontSize: 18, 
    marginTop: 20, 
    alignSelf: 'center'
  }
}