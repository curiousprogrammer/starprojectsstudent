import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Button, Header } from 'react-native-elements';
import { connect } from 'react-redux';

import UploadAnswers from './Upload/UploadAnswers';
import UploadFeedback from './Upload/UploadFeedback';
import UploadImages from './Upload/UploadImages';
import UploadAttendance from './Upload/UploadAttendance';
import AlertModal from '../../components/Common/AlertModal';
import LoaderModal from '../../components/Common/LoaderModal';
import { getData, getStudents, completeAssessment } from '../../common/localSave';
import  { uploadTheoryAnswers, uploadStudentImage, uploadFeedback, uploadAttendance } from '../../actions/uploadActions';
import globalStyles from '../../common/globalStyles';

class UploadDataList extends Component {
  constructor(props) {
    super(props);

    const { centerID, assessmentID } = this.props.navigation.state.params;

    this.state = {
      centerID,
      assessmentID,
      isVisible: false,
      isAnswersVisible: false,
      isFeedbackVisible: false,
      isImagesVisible: false,
      isAttendanceVisible: false,
      showAlert: false,
      assessment: "",
      studentList: [],
      uploadAttendance: 0,
      answers: {},
      uploadAnswers: 0,
      feedback: {},
      uploadFeedback: 0,
      images: [],
      uploadImages: 0,
      uploadError: false
    } 
  }

  async componentDidMount() {
    const { centerID, assessmentID } = this.state;

    const assessmentList = await getData('@upload_data');
    const students = await getStudents(centerID);

    if (assessmentList) {
      const assessment = assessmentList.find((assessmentData) => centerID == assessmentData.assessmentDetails.centerID && assessmentID == assessmentData.assessmentDetails.assessmentID);
      if (assessment) {
        if (!assessment.assessmentDetails.uploaded) {
          const { assessmentData: { answers, feedback, images } } = assessment;
          this.setState({ assessment, answers: Object.values(answers), feedback: Object.values(feedback), images, studentList: students });
        } else {
          this.setState({ assessment });
        }
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { answerUploaded, feedbackUploaded, imageUploaded, attendanceUploaded, uploadError } = this.props;

    if (answerUploaded && answerUploaded != prevProps.answerUploaded) {
      this.setState({ uploadAnswers: ++this.state.uploadAnswers });
    }

    if (feedbackUploaded && feedbackUploaded != prevProps.feedbackUploaded) {
      this.setState({ uploadFeedback: ++this.state.uploadFeedback });
    }

    if (imageUploaded && imageUploaded != prevProps.imageUploaded) {
      this.setState({ uploadImages: ++this.state.uploadImages });
    }

    if (attendanceUploaded && attendanceUploaded != prevProps.attendanceUploaded) {
      this.setState({ uploadAttendance: ++this.state.uploadAttendance });
    }

    if (uploadError && uploadError != prevProps.uploadError) {
      this.setState({ uploadError });
    }
  }

  showUploadData = () => {
    const { assessment } = this.state;

    if (!assessment.assessmentDetails.uploaded) {
      this.setState({ isVisible: !this.state.isVisible });
    }
  }

  uploadAnswer = (item) => {
    this.props.uploadTheoryAnswers(item.answers, item.candidate_id, item.assessment_id);
  }

  uploadFeedback = (item) => {
    this.props.uploadFeedback(item);
  }

  uploadImage = (item) => {
    this.props.uploadStudentImage(item);
  }

  uploadAttendance = (item) => {
    const { centerID, assessmentID } = this.state;

    const attendanceObj = {
      center_id: centerID,
      assessment_id: assessmentID,
      student_id: item.student_id,
      practical: item.practical ? '1' : '0',
      theory: item.theory ? '1' : '0'
    };

    this.props.uploadAttendance(attendanceObj);
  }

  completeUpload = () => {
    const { assessment } = this.state;
    completeAssessment(assessment.assessmentDetails);

    const assessmentCopy = assessment;
    assessmentCopy.assessmentDetails.uploaded = true;

    this.setState({ assessmentList: assessmentCopy, showAlert: false, isVisible: false });
  }

  render() {
    const { assessment, isAnswersVisible, isFeedbackVisible, isImagesVisible, isAttendanceVisible } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Header 
          centerComponent={{ text: 'Upload Data', style: { color: '#fff', fontSize: 16 } }}
          containerStyle={{ paddingTop: 0, height: 50 }}
        />
        <ScrollView style={globalStyles.container}>
          {assessment ?
            (
              <View>
                <TouchableOpacity 
                  style={styles.centerContainer}
                  onPress={() => this.showUploadData(assessment.assessmentDetails.centerID)}
                >
                  <View>
                    <View>
                      <View style={{ flexDirection: 'row'}}>
                        <Text style={styles.centerDetails}>Center ID: {assessment.assessmentDetails.centerID}</Text>
                        <Text style={[styles.statusText, assessment.assessmentDetails.uploaded ? { backgroundColor: 'limegreen' } : { backgroundColor: 'red' }]}>
                          {assessment.assessmentDetails.uploaded ? "Uploaded" : "Not Uploaded"}
                        </Text>
                      </View>
                      <Text style={styles.centerSubDetails}>Assessment Name: {assessment.assessmentDetails.assessmentName}</Text>
                      <Text style={styles.centerSubDetails}>Assessment Duration: {assessment.assessmentDetails.assessmentDuration} minutes</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                  {this.state.isVisible && 
                    (
                      <View style={[styles.centerContainer, { marginBottom: 50 }]}>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Answers: {Object.keys(this.state.answers).length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Answers"
                              onPress={() => this.setState({ isAnswersVisible: !this.state.isAnswersVisible })}
                            />
                          </View>
                          {isAnswersVisible && <UploadAnswers 
                            answers={this.state.answers} 
                            uploadAnswer={this.uploadAnswer}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Feedback: {Object.keys(this.state.feedback).length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Feedback"
                              onPress={() => this.setState({ isFeedbackVisible: !this.state.isFeedbackVisible })}
                            />
                          </View>
                          {isFeedbackVisible && <UploadFeedback 
                            feedback={this.state.feedback} 
                            uploadFeedback={this.uploadFeedback}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Images: {this.state.images.length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Images"
                              onPress={() => this.setState({ isImagesVisible: !this.state.isImagesVisible })}
                            />
                          </View>
                          {isImagesVisible && <UploadImages
                            images={this.state.images}
                            uploadImage={this.uploadImage}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Attendance: {this.state.studentList.length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Attendance"
                              onPress={() => this.setState({ isAttendanceVisible: !this.state.isAttendanceVisible })}
                            />
                          </View>
                          {isAttendanceVisible && <UploadAttendance
                            students={this.state.studentList}
                            uploadAttendance={this.uploadAttendance}
                          />}
                        </View>
                        <Button 
                          title="Complete"
                          containerStyle={{ marginTop: 20 }}
                          buttonStyle={styles.completeButton}
                          onPress={() => this.setState({ showAlert: true })}
                        />
                        {this.props.loading && <LoaderModal isModalVisible={true} />}
                        {this.state.showAlert && 
                          <AlertModal 
                            type="Two"
                            label="Complete and delete all data?" 
                            message="Please make sure to upload all data before continuing with this step! Do you wish to continue?"
                            onPress={this.completeUpload}
                            onCancelPress={() => this.setState({ showAlert: false })}
                          />
                        }
                        {this.state.uploadError && 
                          <AlertModal 
                            type="One"
                            label="Error" 
                            message="Please try again!"
                            onPress={() => this.setState({ uploadError: false })}
                          />
                        }
                      </View>
                    )
                  }
              </View>
            ) : (
              <View>
                <Text style={styles.emptyText}>DATA NOT FOUND!</Text>
              </View>
            )
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = ({ upload }) => {
  return {
    loading: upload.loading,
    uploadError: upload.uploadError,
    answerUploaded: upload.answerUploaded,
    feedbackUploaded: upload.feedbackUploaded,
    imageUploaded: upload.imageUploaded,
    attendanceUploaded: upload.attendanceUploaded
  }
}

export default connect(mapStateToProps, { uploadTheoryAnswers, uploadStudentImage, uploadFeedback, uploadAttendance })(UploadDataList);

const styles = {
  centerContainer: {  
    borderColor: '#D3D3D3',
    borderWidth : 1,
    paddingHorizontal: 15,
    paddingBottom: 15,
    marginVertical: 5
  },
  centerDetails: {
    fontSize: 18,
    marginTop: 15
  },
  centerSubDetails: {
    color: '#919191',
    fontSize: 16
  },
  emptyText: {
    alignSelf: 'center',
    fontSize: 20,
    marginTop: 50
  },
  statusText: {
    color: '#fff',
    paddingHorizontal: 5,
    paddingVertical: 5,
    marginLeft: 10,
    marginTop: 10
  },
  button: {
    width: 120
  },
  completeButton: {
    backgroundColor: 'limegreen',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  }
}