import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { Button } from 'react-native-elements';

const UploadFeedback = (props) => {
  return (
    <FlatList 
      contentContainerStyle={{ paddingBottom: 30 }}
      data={props.feedback}
      renderItem={({ item, index }) => 
        (
          <View style={{ flexDirection: 'row', marginBottom: 15 }}>
            <Text style={styles.text}>{index + 1}. {item.candidateID}</Text>
            <Button 
              title="Upload"
              onPress={() => props.uploadFeedback(item)}
            />
          </View>
        )
      }
      keyExtractor={item => item.candidateID}
    />
  )
}

export default UploadFeedback;

const styles = {
  text: {
    marginRight: 20,
    paddingTop: 10
  }
}