import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { Button } from 'react-native-elements';

const UploadAnswers = (props) => {
  return (
    <FlatList 
      contentContainerStyle={{ paddingBottom: 30 }}
      data={props.answers}
      renderItem={({ item, index }) => 
        (
          <View style={{ flexDirection: 'row', marginBottom: 15 }}>
            <Text style={styles.text}>{index + 1}. {item.candidate_id}</Text>
            <Button 
              title="Upload"
              onPress={() => props.uploadAnswer(item)}
            />
          </View>
        )
      }
      keyExtractor={item => item.candidate_id}
    />
  )
}

export default UploadAnswers;

const styles = {
  text: {
    marginRight: 20,
    paddingTop: 10
  }
}