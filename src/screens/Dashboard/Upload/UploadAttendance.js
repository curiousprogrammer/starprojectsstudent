import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { Button } from 'react-native-elements';

const UploadAttendance = (props) => {
  return (
    <FlatList 
      contentContainerStyle={{ paddingBottom: 30 }}
      data={props.students}
      renderItem={({ item, index }) => 
        (
          <View style={{ flexDirection: 'row', marginBottom: 15 }}>
            <Text style={styles.text}>{index + 1}. {item.candidate_id}</Text>
            <Button 
              title="Upload"
              onPress={() => props.uploadAttendance(item)}
            />
          </View>
        )
      }
      keyExtractor={item => item.candidate_id}
    />
  )
}

export default UploadAttendance;

const styles = {
  text: {
    marginRight: 20,
    paddingTop: 10
  }
}