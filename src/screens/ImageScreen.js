import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import { saveStudentImage } from '../actions/saveActions';

class ImageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assessmentID: ""
    }
  }

  uploadImage = () => {
    const { type, image, candidateID, assessmentID } = this.props.navigation.state.params;
    if (type == 'Annexure') {

    } else if (type == 'Student') {
      this.props.saveStudentImage(image, candidateID, assessmentID);
      this.props.navigation.navigate("TestInfo");
    }
  }

  render() {
    const { image } = this.props.navigation.state.params;
    return (
      <View>
        <View>
          <Image
            style={styles.image}
            source={{ uri: `data:image/png;base64,${image}` }}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button 
            icon={{ name: 'done' }}
            buttonStyle={{ backgroundColor: '#02ad13', marginRight: 20 }}
            title="Upload"
            onPress={this.uploadImage}
          />
          <Button 
            icon={{ name: 'cancel' }}
            buttonStyle={{ backgroundColor: '#dbdbdb' }}
            title="Cancel"
            onPress={() => this.props.navigation.navigate("Camera")}
          />
        </View>
      </View>
    )
  }
}

export default connect(null, { saveStudentImage })(ImageScreen);

const styles = {
  buttonContainer: {
    position: 'absolute',
    bottom: 50,
    right: 60,
    flex: 1,
    flexDirection: 'row'
  },
  image: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
}