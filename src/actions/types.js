export const VERIFY_CENTER = 'verify_center';
export const VERIFY_CENTER_SUCCESS = 'verify_center_success';
export const VERIFY_CENTER_FAIL = 'verify_center_fail';

export const DOWNLOAD_ASSESSMENT = 'download_assessment';
export const DOWNLOAD_ASSESSMENT_SUCCESS = 'download_assessment_success';
export const DOWNLOAD_ASSESSMENT_FAIL = 'download_assessment_fail';



// SAVE
export const SAVE_STUDENT_IMAGE = 'save_student_image';
export const SAVE_STUDENT_IMAGE_SUCCESS = 'save_student_image_success';
export const SAVE_STUDENT_IMAGE_FAIL = 'save_student_image_fail';

export const RESET_SAVED_IMAGE = 'reset_saved_image';

export const SAVE_THEORY_ANSWERS = 'save_theory_answers';
export const SAVE_THEORY_ANSWERS_SUCCESS = 'save_theory_answers_success';
export const SAVE_THEORY_ANSWERS_FAIL = 'save_theory_answers_fail';

export const SAVE_TRAINEE_FEEDBACK = 'save_trainer_feedback';

export const SAVE_COMPLETED_STUDENT = 'save_completed_student';



// UPLOAD
export const UPLOAD_STUDENT_IMAGE = 'upload_student_image';
export const UPLOAD_STUDENT_IMAGE_SUCCESS = 'upload_student_image_success';
export const UPLOAD_STUDENT_IMAGE_FAIL = 'upload_student_image_fail';

export const UPLOAD_THEORY_ANSWERS = 'upload_theory_answers';
export const UPLOAD_THEORY_ANSWERS_SUCCESS = 'upload_theory_answers_success';
export const UPLOAD_THEORY_ANSWERS_FAIL = 'upload_theory_answers_fail';

export const UPLOAD_FEEDBACK = 'upload_feedback';
export const UPLOAD_FEEDBACK_SUCCESS = 'upload_feedback_success';
export const UPLOAD_FEEDBACK_FAIL = 'upload_feedback_fail';

export const UPlOAD_ATTENDANCE = 'upload_attendance';
export const UPlOAD_ATTENDANCE_SUCCESS = 'upload_attendance_success';
export const UPlOAD_ATTENDANCE_FAIL = 'upload_attendance_fail';



// TEST
export const GET_TESTS = 'get_tests';
export const SAVE_ASSESSMENT_DETAILS = 'save_assessment_details';
export const RESET_TEST_SAVED = 'reset_test_saved';