import {
  SAVE_STUDENT_IMAGE,
  RESET_SAVED_IMAGE,
  SAVE_THEORY_ANSWERS,
  SAVE_TRAINEE_FEEDBACK,
  RESET_TEST_SAVED,
  SAVE_COMPLETED_STUDENT
} from './types';
import { storeStudentImages, storeTheoryAnswers, storeTraineeFeedback, storeCompletedStudent } from '../common/localSave';

export const saveStudentImage = (image, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_STUDENT_IMAGE });

      const studentImage = {
        image,
        candidate_id,
        assessment_id,
        timeStamp: Date.now()
      }

      storeStudentImages(studentImage);
      
    } catch (e) {
      console.log(e);
    }
  }
}

export const resetSavedImage = () => {
  return {
    type: RESET_SAVED_IMAGE
  }
}

export const saveTheoryAnswers = (answers, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_THEORY_ANSWERS });

      const theoryAnswers = {
        answers,
        candidate_id,
        assessment_id
      };

      storeTheoryAnswers(theoryAnswers);

      } catch (e) {
      console.log(e);
    }
  }
}

export const resetTestSaved = () => {
  return {
    type: RESET_TEST_SAVED
  }
}

export const saveTraineeFeedback = (traineeFeedback) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_TRAINEE_FEEDBACK });

      storeTraineeFeedback(traineeFeedback);

      } catch (e) {
      console.log(e);
    }
  }
}

export const saveCompletedStudent = (candidateID, centerID) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_COMPLETED_STUDENT });

      storeCompletedStudent(candidateID, centerID);

      } catch (e) {
      console.log(e);
    }
  }
}