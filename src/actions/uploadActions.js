import axios from 'axios';

import {
  UPLOAD_STUDENT_IMAGE,
  UPLOAD_STUDENT_IMAGE_SUCCESS,
  UPLOAD_STUDENT_IMAGE_FAIL,
  UPLOAD_THEORY_ANSWERS,
  UPLOAD_THEORY_ANSWERS_SUCCESS,
  UPLOAD_THEORY_ANSWERS_FAIL,
  UPLOAD_FEEDBACK,
  UPLOAD_FEEDBACK_SUCCESS,
  UPLOAD_FEEDBACK_FAIL,
  UPlOAD_ATTENDANCE,
  UPlOAD_ATTENDANCE_SUCCESS,
  UPlOAD_ATTENDANCE_FAIL,
} from './types';
import { baseURL } from '../endpoint';

export const uploadStudentImage = (image) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_STUDENT_IMAGE });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_student_image');
      bodyFormData.append('candidate_id', image.candidate_id);
      bodyFormData.append('assessment_id', image.assessment_id);
      bodyFormData.append('image', image.image);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_STUDENT_IMAGE_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_STUDENT_IMAGE_FAIL });
      }

    } catch (e) {
      console.log(e);
    }
  }
};

export const uploadTheoryAnswers = (answers, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_THEORY_ANSWERS });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_theory_ansers');
      bodyFormData.append('candidate_id', candidate_id);
      bodyFormData.append('assessment_id', assessment_id);
      bodyFormData.append('answer', JSON.stringify(answers));
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_THEORY_ANSWERS_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_THEORY_ANSWERS_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
};

export const uploadFeedback = (feedback) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_FEEDBACK });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_trainee_feedback');
      bodyFormData.append('trainee_name', feedback.trainee_name);
      bodyFormData.append('trainig_center_name', feedback.trainig_center_name);
      bodyFormData.append('job_roll_end', feedback.job_roll_end);
      bodyFormData.append('center_id', feedback.center_id);
      bodyFormData.append('training_start_date', feedback.training_start_date);
      bodyFormData.append('batch_id', feedback.batch_id);
      bodyFormData.append('trainer_name', feedback.trainer_name);
      bodyFormData.append('student_enrolment_no', feedback.student_enrolment_no);
      bodyFormData.append('about_trainer_training_1', feedback.about_trainer_training_1);
      bodyFormData.append('about_trainer_training_2', feedback.about_trainer_training_2);
      bodyFormData.append('about_trainer_training_3', feedback.about_trainer_training_3);
      bodyFormData.append('about_trainer_training_4', feedback.about_trainer_training_4);
      bodyFormData.append('about_trainer_training_5', feedback.about_trainer_training_5);
      bodyFormData.append('training_vanue_1', feedback.training_vanue_1);
      bodyFormData.append('training_vanue_2', feedback.training_vanue_2);
      bodyFormData.append('pmkvc_1', feedback.pmkvc_1);
      bodyFormData.append('pmkvc_2', feedback.pmkvc_2);
      bodyFormData.append('pmkvc_3', feedback.pmkvc_3);
      bodyFormData.append('pmkvc_4', feedback.pmkvc_4);
      bodyFormData.append('pmkvc_5', feedback.pmkvc_5);
      bodyFormData.append('pmkvc_6', feedback.pmkvc_6);
      bodyFormData.append('pmkvc_7', feedback.pmkvc_7);
      bodyFormData.append('pmkvc_8', feedback.pmkvc_8);
      bodyFormData.append('pmkvc_9', feedback.pmkvc_9);
      bodyFormData.append('training_rate', feedback.training_rate);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_FEEDBACK_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_FEEDBACK_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
};


export const uploadAttendance = (attendance) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPlOAD_ATTENDANCE });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_student_attendance');
      bodyFormData.append('center_id', attendance.center_id);
      bodyFormData.append('assessment_id', attendance.assessment_id);
      bodyFormData.append('student_id', attendance.student_id);
      bodyFormData.append('attendance_theory', attendance.theory);
      bodyFormData.append('attendance_practical', attendance.practical);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPlOAD_ATTENDANCE_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPlOAD_ATTENDANCE_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
};