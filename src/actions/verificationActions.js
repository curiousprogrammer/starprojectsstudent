import axios from 'axios';

import { VERIFY_CENTER, VERIFY_CENTER_SUCCESS, VERIFY_CENTER_FAIL } from './types';
import { baseURL } from '../endpoint';

export const verifyCenter = (verifyCenterObj) => {
  return async (dispatch) => {
    try {
      dispatch({ type: VERIFY_CENTER, payload: verifyCenterObj });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'verify_center');
      bodyFormData.append('center_id', verifyCenterObj.centerID);
      bodyFormData.append('assessor_id', verifyCenterObj.assessorID);
      bodyFormData.append('password', verifyCenterObj.assessorPassword);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: VERIFY_CENTER_SUCCESS, payload: { centerDetails: data.center, message: data.success_msg } });
      } else if (data.status == 0) {
        dispatch({ type: VERIFY_CENTER_FAIL, payload: { message: data.error_msg } });
      }

    } catch (e) {
      console.log(e);
    }
  }
};