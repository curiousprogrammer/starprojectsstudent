import { GET_TESTS, SAVE_ASSESSMENT_DETAILS } from './types';
import * as testData from '../testData.json';

export const getTests = () => { 
  return {
    type: GET_TESTS,
    payload: testData
  }
}

export const saveAssessmentDetails = (assessmentDetails) => {
  return {
    type: SAVE_ASSESSMENT_DETAILS,
    payload: assessmentDetails
  }
}