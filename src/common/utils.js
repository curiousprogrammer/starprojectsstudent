import RNSecureKeyStore, { ACCESSIBLE } from 'react-native-secure-key-store';

export const setData = async (key, value) => {

  let existingData = [];
  let newData = {};

  if (key === 'assessmentID') {
    newData['id'] = value;
  } else {
    newData = value;
  }
  
  try {
    existingData = await getData(key);
    if (existingData) {
      existingData.push(newData);
    } else {
      existingData = [];
      existingData.push(newData);
    }
  } catch (e) {
    existingData.push(newData);
  }
  
  RNSecureKeyStore.set(key, JSON.stringify(existingData), {accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY})
  .then((res) => {
    console.log(res);
  }, (err) => {
    console.log(err);
  });
}

export const getData = (key) => {
  return new Promise((resolve, reject) => {
    RNSecureKeyStore.get(key)
    .then((res) => {
      const result = JSON.parse(res);
      resolve(result);
    }, (err) => {
      console.log(err);
      reject(err);
    });
  })
}

export const setAssessmentData = async (key, value) => {

  let existingData = [];

  try {
    existingData = await getData(key);
    if (existingData) {
      existingData.push(value);
    } else {
      existingData = [];
      existingData.push(value);
    }
  } catch (e) {
    existingData.push(value);
  }
  
  RNSecureKeyStore.set(key, JSON.stringify(existingData), {accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY})
  .then((res) => {
    console.log(res);
  }, (err) => {
    console.log(err);
  });
}

export const getAssessmentData = (key, centerID, assessmentID) => {
  return new Promise((resolve, reject) => {
    RNSecureKeyStore.get(key)
    .then((res) => {
      const allData = JSON.parse(res);
      const result = allData.find((assessmentData) => centerID == assessmentData.center.id && assessmentID == assessmentData.assessment.id);
      resolve(result);
    }, (err) => {
      console.log(err);
      reject(err);
    });
  })
}

export const getDate = (dateObj) => {
  const dd = String(dateObj.getDate()).padStart(2, '0');
  const mm = String(dateObj.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = dateObj.getFullYear();
  const stringDate = dd + '/' + mm + '/' + yyyy;

  return stringDate;
}