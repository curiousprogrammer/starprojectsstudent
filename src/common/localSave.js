import AsyncStorage from '@react-native-community/async-storage';

import store from '../store/index';

const storeStudentImages = async (value) => {
  const storageKey = '@student_images';
  const existingImages = await getData(storageKey);

  if (existingImages && existingImages.length > 0) {
    existingImages.push(value);
    storeData(storageKey, existingImages);
  } else {
    let studentImagesArray = [];
    studentImagesArray.push(value);
    storeData(storageKey, studentImagesArray);
  }
}

const storeTheoryAnswers = async (value) => {
  const storageKey = '@theory_answers';
  const candidateId = value.candidate_id;
  const existingAnswers = await getData(storageKey);
  let answersObj = {
    [candidateId]: value
  };

  if (existingAnswers) {
    const updatedAnswers = { ...existingAnswers, ...answersObj };
    storeData(storageKey, updatedAnswers);
  } else {
    storeData(storageKey, answersObj);
  }
}

const storeTraineeFeedback = async (value) => {
  const storageKey = '@trainee_feedback';
  const candidateId = value.candidateID;
  const existingFeedback = await getData(storageKey);
  let traineeFeedbackObj = {
    [candidateId]: value
  };

  if (existingFeedback) {
    const updatedFeedback = { ...existingFeedback, ...traineeFeedbackObj };
    storeData(storageKey, updatedFeedback);
  } else {
    storeData(storageKey, traineeFeedbackObj);
  }

  storeAllData();
}

const storeAllData = async () => {
  const existingImages = await getData("@student_images");
  const existingAnswers = await getData("@theory_answers");
  const existingFeedback = await getData("@trainee_feedback");

  const { centerID, assessmentID, assessmentName, assessmentDuration } = store.getState().test.assessmentDetails;

  const uploadArray = [];
  const uploadData = {
    assessmentDetails: {
      centerID,
      assessmentID,
      assessmentName,
      assessmentDuration,
      uploaded: false
    },
    assessmentData: {
      images: existingImages,
      answers: existingAnswers,
      feedback: existingFeedback
    }
  };
  uploadArray.push(uploadData);
  
  const storageKey = '@upload_data';
  const existingUploads = await getData(storageKey);

  let updatedUploads;
  if (existingUploads) {
    existingUploads.forEach(assessment => {
      if (assessment.assessmentDetails.centerID === centerID) {
        assessment.assessmentData = { ...uploadData.assessmentData };
        updatedUploads = existingUploads;
      } else {
        updatedUploads = existingUploads.concat(uploadArray);
      }
    });
    storeData(storageKey, updatedUploads);
  } else {
    storeData(storageKey, uploadArray);
  }
}

const completeAssessment = async (assessmentDetails) => {
  const storageKey = '@upload_data';
  const existingUploads = await getData(storageKey);

  const updatedUploads = existingUploads.map(uploads => {
    if (uploads.assessmentDetails.centerID == assessmentDetails.centerID && uploads.assessmentDetails.assessmentID == assessmentDetails.assessmentID) {
      uploads.assessmentDetails.uploaded = true;
      return {
        assessmentDetails: uploads.assessmentDetails
      }
    } else {
      return {
        ...uploads
      }
    }
  });

  storeData(storageKey, updatedUploads);

  AsyncStorage.multiRemove(["@student_images", "@theory_answers", "@trainee_feedback"]);

}

const storeStudents = async (students, centerID, initial) => {
  const storageKey = `@student_attendance`;
  const existingAttendance = await getData(storageKey);

  let modifiedStudents;
  if (initial) {
    modifiedStudents = students.map((student) => {
      return {
        ...student,
        theory: false,
        practical: false
      }
    });
  } else {
    modifiedStudents = students;
  }

  let attendanceObj = {
    [centerID]: modifiedStudents
  };

  if (existingAttendance) {
    const updatedAttendance = { ...existingAttendance, ...attendanceObj };
    storeData(storageKey, updatedAttendance);
  } else {
    storeData(storageKey, attendanceObj);
  }
}

const getData = async (storageKey) => {
  try {
    const jsonValue = await AsyncStorage.getItem(storageKey);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    // error reading value
  }
}

const storeData = async (storageKey, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(storageKey, jsonValue);
  } catch (e) {
    // saving error
  }
}

const getStudents = async (centerID) => {
  try {
    const storageKey = `@student_attendance`;
    const existingAttendance = await getData(storageKey);
    const centerStudents = existingAttendance[centerID];  
    return centerStudents;
  } catch (e) {
    console.log(e);
  }
}

const storeCompletedStudent = async (candidateID, centerID) => {
  try {
    const storageKey = `@student_attendance`;
    const existingAttendance = await getData(storageKey);
    const centerStudents = existingAttendance[centerID];  
    centerStudents.forEach(student => {
      if (student.candidate_id === candidateID) {
        student.theory = true;
      }
    });

    storeStudents(centerStudents, centerID, false);
  } catch (e) {
    console.log(e);
  }
}

export { 
  storeTheoryAnswers, 
  storeStudentImages, 
  storeTraineeFeedback, 
  storeCompletedStudent, 
  completeAssessment, 
  getData, 
  storeStudents, 
  getStudents 
};