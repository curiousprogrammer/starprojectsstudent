import {  
  UPLOAD_STUDENT_IMAGE,
  UPLOAD_STUDENT_IMAGE_SUCCESS,
  UPLOAD_STUDENT_IMAGE_FAIL,
  UPLOAD_THEORY_ANSWERS,
  UPLOAD_THEORY_ANSWERS_SUCCESS,
  UPLOAD_THEORY_ANSWERS_FAIL,
  UPLOAD_FEEDBACK,
  UPLOAD_FEEDBACK_SUCCESS,
  UPLOAD_FEEDBACK_FAIL,
  UPlOAD_ATTENDANCE,
  UPlOAD_ATTENDANCE_SUCCESS,
  UPlOAD_ATTENDANCE_FAIL
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  uploadError: false,
  answerUploaded: false,
  feedbackUploaded: false,
  imageUploaded: false,
  attendanceUploaded: false
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case UPLOAD_THEORY_ANSWERS:
      return { ...state, loading: true, answerUploaded: false, uploadError: false };
    
    case UPLOAD_THEORY_ANSWERS_SUCCESS:
      return { ...state, loading: false, answerUploaded: true, uploadError: false };

    case UPLOAD_THEORY_ANSWERS_FAIL:
      return { ...state, loading: false, answerUploaded: false, uploadError: true };

    case UPLOAD_FEEDBACK: 
      return { ...state, loading: true, feedbackUploaded: false, uploadError: false };

    case UPLOAD_FEEDBACK_SUCCESS:
      return { ...state, loading: false, feedbackUploaded: true, uploadError: false };

    case UPLOAD_FEEDBACK_FAIL:
      return { ...state, loading: false, feedbackUploaded: false, uploadError: true };

    case UPLOAD_STUDENT_IMAGE: 
    return { ...state, loading: true, imageUploaded: false, uploadError: false };

    case UPLOAD_STUDENT_IMAGE_SUCCESS:
      return { ...state, loading: false, imageUploaded: true, uploadError: false };

    case UPLOAD_STUDENT_IMAGE_FAIL:
      return { ...state, loading: false, imageUploaded: false, uploadError: true };

    case UPlOAD_ATTENDANCE: 
      return { ...state, loading: true, attendanceUploaded: false, uploadError: false };

    case UPlOAD_ATTENDANCE_SUCCESS:
      return { ...state, loading: false, attendanceUploaded: true, uploadError: false };

    case UPlOAD_ATTENDANCE_FAIL:
      return { ...state, loading: false, attendanceUploaded: false, uploadError: true };

    default:
      return state;
  }
}