import { DOWNLOAD_ASSESSMENT, DOWNLOAD_ASSESSMENT_SUCCESS, DOWNLOAD_ASSESSMENT_FAIL } from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  centerDetails: null,
  error: false,
  errorMsg: null,
  centerObj: null,
  downloaded: false
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case DOWNLOAD_ASSESSMENT: 
      return { ...state, loading: true, error: false, downloaded: false };

    case DOWNLOAD_ASSESSMENT_SUCCESS:
      return { ...state, loading: false, error: false, downloaded: true };

    case DOWNLOAD_ASSESSMENT_FAIL:
      return { ...state, loading: false, error: true, errorMsg: action.payload.message, downloaded: false };

    default:
      return state;
  }
}