import { GET_TESTS, SAVE_ASSESSMENT_DETAILS } from '../actions/types';

const INITIAL_STATE = {
  testData: null,
  assessmentDetails: null
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case GET_TESTS: 
      return { ...state, testData: action.payload };

    case SAVE_ASSESSMENT_DETAILS:
      return { ...state, assessmentDetails: action.payload };
    
    default:
      return state;
  }
}