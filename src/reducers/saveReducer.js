import {  
  SAVE_STUDENT_IMAGE,
  RESET_SAVED_IMAGE, 
  SAVE_THEORY_ANSWERS,
  RESET_TEST_SAVED
} from '../actions/types';

const INITIAL_STATE = {
  savedImage: false,
  testSaved: false
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case SAVE_STUDENT_IMAGE:
      return { ...state, savedImage: true };

    case RESET_SAVED_IMAGE: {
      return { ...state, savedImage: false };
    }
    
    case SAVE_THEORY_ANSWERS:
      return { ...state, testSaved: true };

    case RESET_TEST_SAVED:
      return { ...state, testSaved: false };

    default:
      return state;
  }
}