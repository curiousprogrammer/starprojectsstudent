import React from 'react';
import { Provider } from 'react-redux';

import store from './store';
import AppContainer from './components/Navigation';

// import AsyncStorage from '@react-native-community/async-storage';
// import RNSecureKeyStore, { ACCESSIBLE } from 'react-native-secure-key-store';

// AsyncStorage.clear();

// RNSecureKeyStore.remove("assessmentID")
// 	.then((res) => {
// 		console.log(res);
// 	}, (err) => {
// 		console.log(err);
// 	});
  
//   RNSecureKeyStore.remove("assessorData")
// 	.then((res) => {
// 		console.log(res);
// 	}, (err) => {
// 		console.log(err);
//   });

//   RNSecureKeyStore.remove("assessmentData")
// 	.then((res) => {
// 		console.log(res);
// 	}, (err) => {
// 		console.log(err);
//   });

//   RNSecureKeyStore.remove("batchData")
// 	.then((res) => {
// 		console.log(res);
// 	}, (err) => {
// 		console.log(err);
//   });

//   RNSecureKeyStore.remove("center")
// 	.then((res) => {
// 		console.log(res);
// 	}, (err) => {
// 		console.log(err);
//   });

  
const App = () => {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  )
}

export default App;